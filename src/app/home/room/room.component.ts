import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(private _formBuilder: FormBuilder) {}

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      roomCount: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      adultCount: ['', Validators.required],
      childCount: ['','' ],
      childAge: ['', ''],
    });
  }

}
